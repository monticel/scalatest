import akka.actor.ActorSystem
import akka.testkit.{TestActorRef, TestKit}
import org.scalatest.WordSpecLike
import scala.concurrent.duration._

class EnvironmentSpec extends TestKit(ActorSystem()) with WordSpecLike {
  val environment = TestActorRef(new Environment)

  "Environment " must {
    "forward messages to the destination" in {
      environment ! ("message", testActor)
      expectMsg("message")
    }
    "discard messages without destination" in {
      environment ! "message"
      expectNoMsg(1 second)
    }
  }
}