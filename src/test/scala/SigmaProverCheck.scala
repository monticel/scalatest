import java.math.BigInteger

import User.{SigmaFirstMsg, SigmaSecondMsg}
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit}
import edu.biu.scapi.interactiveMidProtocols.sigmaProtocol.damgardJurikProduct.{SigmaDJProductCommonInput, SigmaDJProductProverComputation, SigmaDJProductProverInput, SigmaDJProductVerifierComputation}
import edu.biu.scapi.midLayer.asymmetricCrypto.encryption.{DJKeyGenParameterSpec, ScDamgardJurikEnc}
import edu.biu.scapi.midLayer.asymmetricCrypto.keys.{DamgardJurikPrivateKey, DamgardJurikPublicKey}
import edu.biu.scapi.midLayer.ciphertext.BigIntegerCiphertext
import edu.biu.scapi.midLayer.plaintext.BigIntegerPlainText
import org.scalacheck.Prop.{BooleanOperators, forAll}
import org.scalacheck.Properties

import scala.concurrent.duration._


class SigmaProverCheck extends Properties("SigmaProver") {

  property("Return first and second messages as expected") =

    // Integers <= 0 give this exception on djEnc.encrypt
    // Exception: java.lang.IllegalArgumentException: Message too big for encryption
    forAll { (a: Int, b: Int) => (a > 0 && b > 0) ==> {
      val djEnc = new ScDamgardJurikEnc()
      val keyPair = djEnc.generateKey(new DJKeyGenParameterSpec())
      djEnc.setKey(keyPair.getPublic)

      val aBig = new BigInteger(a.toString)
      val bBig = new BigInteger(b.toString)
      val cBig = aBig.multiply(bBig)

      /* DamgardJurik encrypted numbers */
      val a_C = djEnc.encrypt(new BigIntegerPlainText(aBig))
      val b_C = djEnc.encrypt(new BigIntegerPlainText(bBig))
      val c_C = djEnc.encrypt(new BigIntegerPlainText(cBig))

      val proverInput = new SigmaDJProductProverInput(
        keyPair.getPublic.asInstanceOf[DamgardJurikPublicKey],
        a_C.asInstanceOf[BigIntegerCiphertext],
        b_C.asInstanceOf[BigIntegerCiphertext],
        c_C.asInstanceOf[BigIntegerCiphertext],
        keyPair.getPrivate.asInstanceOf[DamgardJurikPrivateKey],
        new BigIntegerPlainText(aBig),
        new BigIntegerPlainText(bBig)
      )

      val commonInput = new SigmaDJProductCommonInput(
        keyPair.getPublic.asInstanceOf[DamgardJurikPublicKey],
        a_C.asInstanceOf[BigIntegerCiphertext],
        b_C.asInstanceOf[BigIntegerCiphertext],
        c_C.asInstanceOf[BigIntegerCiphertext]
      )

      val prover = new SigmaDJProductProverComputation()
      val verifier = new SigmaDJProductVerifierComputation()
      verifier.sampleChallenge()
      val challenge = verifier.getChallenge

      new TestKit(ActorSystem("TestActorSystem")) with ImplicitSender {
        val prover = system.actorOf(SigmaProver.props(self, proverInput))
        prover ! Broker.SigmaSyn
        prover ! Broker.SigmaChallenge(challenge)

        val firstMsg = receiveOne(5 seconds) match {
          case (SigmaFirstMsg(firstMsg), self) => firstMsg
          case _ => null
        }
        val secondMsg = receiveOne(5 seconds) match {
          case (SigmaSecondMsg(secondMsg), self) => secondMsg
          case _ => null
        }

        assert(verifier.verify(commonInput, firstMsg, secondMsg))
        expectNoMsg(200 milliseconds)

        system.terminate()
      }
      true
    }
    }
}
