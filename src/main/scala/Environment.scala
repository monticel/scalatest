import akka.actor.{Actor, ActorRef}
import akka.event.Logging

class Environment extends Actor{
  val log = Logging(context.system, this)

  override def receive = {
    case (msg, destination: ActorRef) => {
      log.info(sender().path + " -> " + destination.path + " : " + msg.getClass)
      destination forward msg
    }
    case msg @ _ =>
      log.warning("Msg from: " + sender() + " has no destination. Msg: " + msg)
  }
}
