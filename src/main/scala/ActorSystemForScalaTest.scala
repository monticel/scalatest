import akka.actor.{ActorSystem, Props}
import edu.biu.scapi.midLayer.asymmetricCrypto.encryption.{DJKeyGenParameterSpec, ScDamgardJurikEnc}

class ActorSystemForScalaTest(a: Int, b: Int) {
  val djEnc = new ScDamgardJurikEnc()
  val asymmetricKey = djEnc.generateKey(new DJKeyGenParameterSpec())

  val system = ActorSystem("actorSystem")

  val environment = system.actorOf(Props[Environment], "environment")
  val user1 = system.actorOf(User.props(environment, a, asymmetricKey.getPublic), "userA")
  val user2 = system.actorOf(User.props(environment, b, asymmetricKey.getPublic), "userB")
  val broker = system.actorOf(Broker.props(environment, user1, user2, asymmetricKey), "broker")
  broker ! Broker.Begin
}

object Main {
  def main(args: Array[String]): Unit = {
    val actorSystem = new ActorSystemForScalaTest(30, 42)
  }
}