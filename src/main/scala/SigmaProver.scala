import Broker._
import User.{SigmaFirstMsg, SigmaSecondMsg}
import akka.actor.{Actor, ActorRef, Props}
import edu.biu.scapi.interactiveMidProtocols.sigmaProtocol.damgardJurikProduct.{SigmaDJProductProverComputation, SigmaDJProductProverInput}

/** Implements the Prover in the Damgard-Jurik Sigma Verification Protocol.
  *
  * @param environment should forward messages of the form (msg, destination)
  *                    to destination, keeping the sender intact
  * @param sigmaDJProductProverInput publicKey, c1, c2, c3, privateKey, a, b
  */
class SigmaProver(environment: ActorRef,
                  sigmaDJProductProverInput: SigmaDJProductProverInput) extends Actor {
  val sigmaDJProductProverComputation = new SigmaDJProductProverComputation()

  override def receive: Receive = sigmaWaitSyn

  def sigmaWaitSyn: Receive = {
    case SigmaSyn =>
      val firstMsg = sigmaDJProductProverComputation.computeFirstMsg(sigmaDJProductProverInput)
      environment ! (SigmaFirstMsg(firstMsg), sender())
      context become sigmaWaitChallenge
  }

  def sigmaWaitChallenge: Receive = {
    case SigmaChallenge(challenge) =>
      val secondMsg = sigmaDJProductProverComputation.computeSecondMsg(challenge)
      environment ! (SigmaSecondMsg(secondMsg), sender())
      context stop self
  }
}

object SigmaProver {
  def props(environment: ActorRef,
            sigmaDJProductProverInput: SigmaDJProductProverInput) =
    Props(new SigmaProver(environment, sigmaDJProductProverInput))
}
