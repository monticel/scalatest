import java.math.BigInteger
import java.security.PublicKey

import Broker.SigmaChallenge
import User._
import akka.actor.{Actor, ActorRef, Props}
import akka.event.Logging
import edu.biu.scapi.interactiveMidProtocols.sigmaProtocol.damgardJurikProduct.{SigmaDJProductCommonInput, SigmaDJProductVerifierComputation}
import edu.biu.scapi.interactiveMidProtocols.sigmaProtocol.utility.SigmaProtocolMsg
import edu.biu.scapi.midLayer.asymmetricCrypto.encryption.ScDamgardJurikEnc
import edu.biu.scapi.midLayer.asymmetricCrypto.keys.ScDamgardJurikPublicKey
import edu.biu.scapi.midLayer.ciphertext.{AsymmetricCiphertext, BigIntegerCiphertext}
import edu.biu.scapi.midLayer.plaintext.BigIntegerPlainText


/** Implements the Users. See `test specifications`
  *
  * @param environment is expected to forward messages of the form (msg, destination)
  *                    to destination, keeping the sender intact
  * @param number      number to transmit to the broker
  * @param publicKey   broker public key for Damgard-Jurik encryption
  */
class User(environment: ActorRef, number: Int, publicKey: PublicKey) extends Actor {
  val log = Logging(context.system, this)
  val sigmaDJProductVerifierComputation = new SigmaDJProductVerifierComputation()

  val djNumber: AsymmetricCiphertext = {
    val djEnc = new ScDamgardJurikEnc()
    djEnc.setKey(publicKey)
    djEnc.encrypt(new BigIntegerPlainText(new BigInteger(number.toString)))
  }

  /* Behaviours */
  override def receive = initialState

  def initialState: Receive = {
    case BrokerSyn =>
      environment ! (Broker.DJNumber(djNumber), sender())
      context become waitingDJNumbers
  }

  def waitingDJNumbers: Receive = {
    case BrokerDJNumbers(a, b, c) =>
      environment ! (Broker.SigmaSyn, sender())
      context become SigmaWaitFirstMsg(a, b, c)
  }

  def SigmaWaitFirstMsg(a: AsymmetricCiphertext, b: AsymmetricCiphertext, c: AsymmetricCiphertext): Receive = {
    case SigmaFirstMsg(firstMsg) =>
      sigmaDJProductVerifierComputation.sampleChallenge()
      val challenge = sigmaDJProductVerifierComputation.getChallenge
      environment ! (SigmaChallenge(challenge), sender())
      context become SigmaWaitSecondMsg(a, b, c, firstMsg)
  }

  def SigmaWaitSecondMsg(a: AsymmetricCiphertext, b: AsymmetricCiphertext, c: AsymmetricCiphertext, firstMsg: SigmaProtocolMsg): Receive = {
    case SigmaSecondMsg(secondMsg) =>
      val sigmaDJProductCommonInput = new SigmaDJProductCommonInput(
        publicKey.asInstanceOf[ScDamgardJurikPublicKey],
        a.asInstanceOf[BigIntegerCiphertext],
        b.asInstanceOf[BigIntegerCiphertext],
        c.asInstanceOf[BigIntegerCiphertext]
      )
      val verification: Boolean = sigmaDJProductVerifierComputation.verify(sigmaDJProductCommonInput, firstMsg, secondMsg)
      log.info("VERIFICATION: " + verification)
  }
}

object User {
  def props(environment: ActorRef, number: Int, publicKey: PublicKey): Props =
    Props(new User(environment, number, publicKey))

  /* Messages */
  case class BrokerSyn()
  case class BrokerDJNumbers(a: AsymmetricCiphertext, b: AsymmetricCiphertext, c: AsymmetricCiphertext)
  case class BrokerProduct(product: AsymmetricCiphertext)
  case class SigmaFirstMsg(firstMsg: SigmaProtocolMsg)
  case class SigmaSecondMsg(secondMsg: SigmaProtocolMsg)
}
