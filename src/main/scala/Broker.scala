import java.math.BigInteger
import java.security.KeyPair

import Broker._
import akka.actor.{Actor, ActorRef, Cancellable, Props}
import akka.event.Logging
import edu.biu.scapi.interactiveMidProtocols.sigmaProtocol.damgardJurikProduct.SigmaDJProductProverInput
import edu.biu.scapi.midLayer.asymmetricCrypto.encryption.ScDamgardJurikEnc
import edu.biu.scapi.midLayer.asymmetricCrypto.keys.{DamgardJurikPrivateKey, DamgardJurikPublicKey}
import edu.biu.scapi.midLayer.ciphertext.{AsymmetricCiphertext, BigIntegerCiphertext}
import edu.biu.scapi.midLayer.plaintext.BigIntegerPlainText

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/** Implementation of the Broker. See `test exercise` for the specification.
  *
  * @param environment should forward messages of the form (msg, destination)
  *                    to destination, keeping the sender intact
  * @param userA       embodies Alice in the protocol
  * @param userB       embodies Bob in the protocol
  */
class Broker(environment: ActorRef, userA: ActorRef, userB: ActorRef,
             keyPair: KeyPair) extends Actor {
  val log = Logging(context.system, this)
  val users = List(userA, userB)
  val djEnc = new ScDamgardJurikEnc()

  /* Plain text values */
  var a: BigInteger = null
  var b: BigInteger = null
  var c: BigInteger = null

  /* Damgrad-Jurik encrypted values */
  var a_C: AsymmetricCiphertext = null
  var b_C: AsymmetricCiphertext = null
  var c_C: AsymmetricCiphertext = null

  override def preStart(): Unit = {
    djEnc.setKey(keyPair.getPublic)
  }

  /* Behaviours */
  override def receive: Receive = initialState

  def initialState: Receive = {
    case Begin =>
      users foreach (environment ! (User.BrokerSyn, _))
      context become waitingForFirstDJNumber
  }

  def waitingForFirstDJNumber: Receive = {
    case DJNumber(n) =>
      val timeout = context.system.scheduler.scheduleOnce(5 seconds, self, Timeout)
      if (sender == userA) a_C = n else b_C = n
      context become waitingForSecondDJNumber(timeout)
  }

  def waitingForSecondDJNumber(timeout: Cancellable): Receive = {
    case DJNumber(n) =>
      timeout.cancel()
      if (sender == userA) a_C = n else b_C = n
      djEnc.setKey(keyPair.getPublic, keyPair.getPrivate)
      a = djEnc.decrypt(a_C).asInstanceOf[BigIntegerPlainText].getX
      b = djEnc.decrypt(b_C).asInstanceOf[BigIntegerPlainText].getX
      c = a.multiply(b)
      c_C = djEnc.encrypt(new BigIntegerPlainText(c))
      users foreach (environment ! (User.BrokerDJNumbers(a_C, b_C, c_C), _))
      context become sigmaWaitSyn
  }

  def sigmaWaitSyn: Receive = {
    case s@SigmaSyn =>
      val sigmaDJProductProverInput =
        new SigmaDJProductProverInput(
          keyPair.getPublic.asInstanceOf[DamgardJurikPublicKey],
          a_C.asInstanceOf[BigIntegerCiphertext],
          b_C.asInstanceOf[BigIntegerCiphertext],
          c_C.asInstanceOf[BigIntegerCiphertext],
          keyPair.getPrivate.asInstanceOf[DamgardJurikPrivateKey],
          new BigIntegerPlainText(a),
          new BigIntegerPlainText(b)
        )

      // spawn a child for each Verification request
      val prover = context.actorOf(SigmaProver.props(environment, sigmaDJProductProverInput))
      prover forward s
  }

  override def unhandled(message: Any): Unit = {
    message match {
      case Broker.Timeout =>
        log.warning("Timeout received")
        context stop self
      case _ =>
        log.error("From: " + sender() + " unrecognized message: " + message)
    }
  }
}

object Broker {
  def props(environment: ActorRef, user1: ActorRef, user2: ActorRef, keyPair: KeyPair) =
    Props(new Broker(environment, user1, user2, keyPair))

  /* messages */
  case object Begin
  case class DJNumber(number: AsymmetricCiphertext)
  case class EncryptedInt(number: BigInteger)
  case object SigmaSyn
  case class SigmaChallenge(challenge: Array[Byte])
  case object Timeout
}
