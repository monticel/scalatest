name := "scalatest"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.11",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.11",
  "org.scalatest" % "scalatest_2.11" % "3.0.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.13.3" % "test"
)

testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-verbosity", "3")

