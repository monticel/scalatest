#ActorSystemForScalaTest

The file ActorSystemForScalaTest.scala contains the main method. It can be run
as an example


# Protocol simplification

Broker knows the key pair (pk, sk), UserA and UserB only know the public key
(pk) and their number (a or b).
The protocol can thus be simplified:

                                
    Broker  --------------------------------------------------------------
             |        ^                       | 
             |        |                       |
            SYN    D(a, pk)    (D(a, pk), D(b, pk), D(c, pk))      SIGMA
             |        |                       |
             v        |                       v
    UserA   ---------------------------------------------------------------



Assuming the integers (a, b) are encrypted with the Broker's public key, he can
directly decrypt them and calculate c. There is no need for the users to send
again their integers encrypted with a different algorithm.

The complete protocol version is implemented in the `complete` branch, while
the simplified version in the `master` branch.
